package p02;

import java.util.Arrays;

public class BSearch {

  // recursive version with pivot changing
  public static int chop_recur(int target, int[] arr) {
    if (arr.length < 1) return -1;
    return chop_recur_helper(target, arr, 0, arr.length);
  }

  static int chop_recur_helper(int target, int[] arr, int left, int right) {
    // no more element to search
    if (left >= right) return -1;

    int mid_idx = (left + right) / 2;
    int middle = arr[mid_idx];
    // found
    if (middle == target) return mid_idx;
    // move to left half
    else if (target < middle)
      return chop_recur_helper(target, arr, left, mid_idx);
    // move to right half
    else // target > middle
      return chop_recur_helper(target, arr, mid_idx + 1, right);
  }

  // iterative version
  public static int chop_iter(int target, int[] arr) {
    if (arr.length < 1) return -1;
    int left = 0;
    int right = arr.length;
    while (left < right) {
      int mid_idx = (left + right) / 2;
      int middle = arr[mid_idx];
      if (middle == target) return mid_idx;
      else if (target < middle)
        right = mid_idx;
      else // target > middle
        left = mid_idx + 1;
    }
    return -1;
  }

  // recursive version with slicing
  public static int chop_slice(int target, int[] arr) {
    return chop_slice_helper(target, arr, 0);
  }

  public static int chop_slice_helper(int target, int[] arr, int offset) {
    if (arr.length < 1) return -1;
    int mid_idx = arr.length / 2;
    int middle = arr[mid_idx];
    if (middle == target) return mid_idx + offset;
    // move to left half
    else if (target < middle) {
      int[] left_half = Arrays.copyOfRange(arr, 0, mid_idx);
      return chop_slice_helper(target, left_half, offset);
    }
    // move to right half
    else {
      int[] right_half = Arrays.copyOfRange(arr, mid_idx + 1, arr.length);
      return chop_slice_helper(target, right_half, offset + mid_idx + 1);
    }
  }

}
