package p13;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

public class LoC {

  // not to consider /* or */ inside "string"
  public static String rmQuotes(String str) {
    char[] ca = str.toCharArray();
    StringBuilder buf = new StringBuilder();
    boolean in_quote = false;
    int j = 0;
    for (int i = 0; i < ca.length; i++) {
      if (ca[i] == '"') {
        if (in_quote) in_quote = false;
        else in_quote = true;
      } else {
        if (!in_quote)
          buf.append(ca[i]);
      }
    }
    return buf.toString();
  }

  public static int count(String f) {
    BufferedReader br = null;
    List<String> lines = new ArrayList();
    try {
      br = new BufferedReader(new FileReader(f));
      String line;
      while ( (line = br.readLine()) != null) {
        lines.add(line);
      }
    } catch (IOException e) {
    } finally {
      try {
        if (br != null) br.close();
      } catch (IOException e) {
      }
    }

    int cnt = 0;
    boolean in_comment = false;
    for (String line : lines) {
      // remove inner string which may include /* or */
      String wo_quote = rmQuotes(line);
      String prv = "";
      String cur = wo_quote;
      // trim out comments as much as possible
      while (prv != cur) {
        prv = cur;
		if (in_comment) {
		  int end_comment = cur.indexOf("*/");
		  if (end_comment >= 0) {
            in_comment = false;
            cur = cur.substring(end_comment+2, cur.length());
		  }
		} else {
		  int start_comment = cur.indexOf("/*");
		  if (start_comment >= 0) {
            in_comment = true;
            cur = cur.substring(start_comment+2, cur.length());
		  }
		}
      }
      // if inside the comment, move to the next line
      if (in_comment) continue;

      // remove line comment
      int line_comment = cur.indexOf("//");
      if (line_comment >= 0) {
        cur = cur.substring(0, line_comment);
      }

      // finally, trim out leading and trailing white spaces as well as \n
      cur = cur.trim();
      // then check whether the line contains meaningful characters
      if (cur.length() > 0) cnt++;
    }

    return cnt;
  }

}
