package util;

public class Pair<A, B> {
  public final A car;
  public final B cdr;

  public Pair(A car, B cdr) {
    this.car = car;
    this.cdr = cdr;
  }

  @Override
  public String toString() {
    return "(" + car + ", " + cdr + ")";
  }
}
