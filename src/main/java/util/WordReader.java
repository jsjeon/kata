package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.Set;
import java.util.TreeSet;

public class WordReader {

  static Set<String> readWords(String f) {
    BufferedReader br = null;
    Set<String> words = new TreeSet<String>();
    String line;
    try {
      br = new BufferedReader(new FileReader(f));
      while ( (line = br.readLine()) != null) {
        words.add(line.trim());
      }
    } catch (IOException e) {
    } finally {
      try {
        if (br != null) br.close();
      } catch (IOException e) {
      }
    }
    return words;
  }

  public static Set<String> readWords() {
    ClassLoader cl = WordReader.class.getClassLoader();
    String f = cl.getResource("words.txt").getFile();
    return readWords(f);
  }

}
