package p08;

import util.Pair;

import java.util.Set;
import java.util.List;
import java.util.LinkedList;

public class Compound {

  // extendible: multiple concatenation and variable length of words
  public static List<String>
      decomposeExt(Set<String> words, String s)
  {
    int l = s.length();
    if (l <= 1) return null;

    for (int i = 1; i < l; i++) {
      String prefix = s.substring(0, i);
      String suffix = s.substring(i, l);
      if (words.contains(prefix)) {
        List<String> rest = decomposeExt(words, suffix);
        if (rest != null) {
          List<String> upd = new LinkedList(rest);
          upd.add(0, prefix);
          return upd;
        }
      }
    }
    List<String> singleton = new LinkedList<String>();
    singleton.add(s);
    return singleton;
  }

  // fast
  // suppose s.length() == 6
  // choose likely decomposable pivots first
  // substring suffix only when prefix is in the dictionary
  public static Pair<String, String>
      decomposeFast(Set<String> words, String s)
  {
    int l = s.length();
    if (l <= 1) return null;

    int[] pivots = { 3, 2, 4, 1, 5 };
    for (int i : pivots) {
      String prefix = s.substring(0, i);
      if (words.contains(prefix)) {
        String suffix = s.substring(i, l);
        if (words.contains(suffix))
          return new Pair<String, String>(prefix, suffix);
      }
    }
    return null;
  }

  // readable: variable length of words
  // NOTE: this is also fast enough, as long as words are given as a set
  public static Pair<String, String>
      decomposeReadable(Set<String> words, String s)
  {
    int l = s.length();
    if (l <= 1) return null;

    for (int i = 1; i < l; i++) {
      String prefix = s.substring(0, i);
      String suffix = s.substring(i, l);
      if (words.contains(prefix) && words.contains(suffix)) {
        return new Pair<String, String>(prefix, suffix);
      }
    }
    return null;
  }

}
