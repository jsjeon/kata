package p06;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Anagram {

  public static boolean check(String... strs) {
    int sz = strs.length;
    if (sz <= 1) return true;

    int len = -1;
    char[] prv = null;
    for (String str : strs) {
      if (prv == null) { // 1st one
        len = str.length();
        prv = str.toCharArray();
        Arrays.sort(prv);
        continue;
      }
      // o.w. compare with the previous one
      if (str.length() != len) return false;
      char[] cur = str.toCharArray();
      Arrays.sort(cur);
      for (int i = 0; i < len; i++) {
        if (prv[i] != cur[i]) return false;
      }
    }
    return true;
  }

  static void permutation(String prefix, String str, Set<String> res) {
    int l = str.length();
    if (l == 0) res.add(prefix);
    else {
      for (int i = 0; i < l; i++) {
        String next_prefix = prefix + str.charAt(i);
        String remaining = str.substring(0, i) + str.substring(i+1, l);
        permutation(next_prefix, remaining, res);
      }
    }
  }

  public static Set<String> find(Set<String> words, String x) {
    Set<String> perms = new TreeSet<String>();
    permutation("", x, perms);

    Set<String> res = new TreeSet<String>();
    for (String perm : perms) {
      if (words.contains(perm)) res.add(perm);
    }

    /*
    for (String word : res) {
      System.out.print(word + " ");
    }
    if (!res.isEmpty()) System.out.println("");
    */

    return res;
  }

}
