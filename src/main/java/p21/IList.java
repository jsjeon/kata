package p21;

public interface IList<T> {
  public INode<T> find(T elt);
  public void add(T elt);
  public void delete(INode n);
  public T[] values();
}
