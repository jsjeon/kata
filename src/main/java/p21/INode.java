package p21;

public interface INode<T> {
  public T value();
}
