package p21;

import java.lang.reflect.Array;

// without reference
public class AList<T> implements IList<T> {

  class Node<T> implements INode<T> {
    T elt;

    public Node(T elt) {
      this.elt = elt;
    }

    public T value() {
      return elt;
    }
  }

  int idx = 0;
  int pool = 0;
  Node<T>[] _arr;

  public AList() {
    _arr = null;
  }

  public INode<T> find(T elt) {
    if (_arr == null) return null;

    for (int i = 0; i < idx; i++) {
      if (_arr[i].value() == elt) return _arr[i];
    }
    return null;
  }

  public void add(T elt) {
   Node<T> n = new Node<T>(elt);
    if (idx + 1 >= pool) { // realloc
      pool += 32;
      @SuppressWarnings("unchecked")
      Node<T>[] _realloc = (Node<T>[])(Array.newInstance(n.getClass(), pool));
      for (int i = 0; i < idx; i++) {
        _realloc[i] = _arr[i];
      }
      _arr = _realloc;
    }
    _arr[idx++] = n;
  }

  public void delete(INode n) {
    Node<T> nn = (Node<T>)n;
    int i = 0;
    // locate the given item 
    while (i < idx) {
      if (_arr[i] == nn) break;
      i++;
    }
    if (i >= idx) return;
    // shift behind items
    while (i < idx) {
      _arr[i] = _arr[i+1];
      i++;
    }
    _arr[i] = null;
    idx--;
  }

  public T[] values() {
    if (idx == 0) {
      Object[] empty = {};
      return (T[])empty;
    }

    T t = _arr[0].value();
    @SuppressWarnings("unchecked")
    final T[] arr = (T[])(Array.newInstance(t.getClass(), idx));

    for (int i = 0; i < idx; i++) {
      arr[i] = _arr[i].value();
    }
    return arr;
  }

}
