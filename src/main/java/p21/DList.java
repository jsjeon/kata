package p21;

import java.lang.reflect.Array;

// doubly linked list
public class DList<T> implements IList<T> {

  class Node<T> implements INode<T> {
    T elt;
    Node<T> prev;
    Node<T> next;

    public Node(T elt) {
      this.elt = elt;
      prev = null;
      next = null;
    }

    public T value() {
      return elt;
    }
  }

  Node<T> head;
  Node<T> tail;

  public DList() {
    head = null;
    tail = null;
  }

  public INode<T> find(T elt) {
    if (head == null) return null;

    Node<T> iter = head;
    while (iter != null) {
      if (iter.value() == elt) return iter;
      iter = iter.next;
    }
    return null;
  }

  public void add(T elt) {
    Node<T> n = new Node<T>(elt);
    if (head == null) { // zero element
      head = n;
      tail = n;
    } else {
      n.prev = tail;
      tail.next = n;
      tail = n;
    }
  }

  public void delete(INode n) {
    if (head == null) return;

    Node<T> nn = (Node<T>)n;
    if (nn.prev != null) {
      nn.prev.next = nn.next;
    } else { // head
      head = nn.next;
    }
    if (nn.next != null) {
      nn.next.prev = nn.prev;
    } else { // tail
      tail = nn.prev;
    }
  }

  public T[] values() {
    if (head == null) {
      Object[] empty = {};
      return (T[])empty;
    }

    int cnt = 0;
    Node<T> iter = head;
    while (iter != null) {
      cnt++;
      iter = iter.next;
    }

    T t = head.value();
    @SuppressWarnings("unchecked")
    final T[] arr = (T[])(Array.newInstance(t.getClass(), cnt));
    int idx = 0;
    iter = head;
    while (iter != null) {
      arr[idx++] = iter.value();
      iter = iter.next;
    }
    return arr;
  }

}
