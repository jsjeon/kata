package p21;

import java.lang.reflect.Array;

// singly linked list
public class SList<T> implements IList<T> {

  class Node<T> implements INode<T> {
    T elt;
    Node<T> next;

    public Node(T elt) {
      this.elt = elt;
      next = null;
    }

    public T value() {
      return elt;
    }
  }

  Node<T> head;

  public SList() {
    head = null;
  }

  public INode<T> find(T elt) {
    if (head == null) return null;

    Node<T> iter = head;
    while (iter != null) {
      if (iter.value() == elt) return iter;
      iter = iter.next;
    }
    return null;
  }

  public void add(T elt) {
    Node<T> n = new Node<T>(elt);
    if (head == null) { // zero element
      head = n;
    } else {
      Node<T> tail = head;
      while (tail.next != null)
        tail = tail.next;
      tail.next = n;
    }
  }

  public void delete(INode n) {
    // zero element
    if (head == null) return;

    Node<T> nn = (Node<T>)n;
    Node<T> prev = null;
    Node<T> curr = head;
    while (curr != null) {
      if (curr == nn) {
        if (prev == null) { // only one element
          head = curr.next; // reset the head
        } else {
          prev.next = curr.next;
        }
      }
      prev = curr;
      curr = curr.next;
    }
  }

  public T[] values() {
    if (head == null) {
      Object[] empty = {};
      return (T[])empty;
    }

    int cnt = 0;
    Node<T> iter = head;
    while (iter != null) {
      cnt++;
      iter = iter.next;
    }

    T t = head.value();
    @SuppressWarnings("unchecked")
    final T[] arr = (T[])(Array.newInstance(t.getClass(), cnt));
    int idx = 0;
    iter = head;
    while (iter != null) {
      arr[idx++] = iter.value();
      iter = iter.next;
    }
    return arr;
  }

}
