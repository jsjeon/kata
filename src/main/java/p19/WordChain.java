package p19;

import java.util.Set;
import java.util.List;
import java.util.LinkedList;

public class WordChain {

  // count how many characters are different
  static int diff(String s1, String s2) {
    int l1 = s1.length();
    int l2 = s2.length();
    String prv = l1 > l2 ? s2 : s1; // smaller
    String cur = l1 > l2 ? s1 : s2; // bigger

    int cnt = 0;
    for (int i = 0; i < prv.length(); i++) {
      if (prv.charAt(i) != cur.charAt(i)) cnt++;
    }
    return cnt + (cur.length() - prv.length());
  }

  // "differ from the previous word by just one letter"
  public static boolean linkable(String prv, String cur) {
    int l1 = prv.length();
    int l2 = cur.length();
    if (l1 - l2 > 2 || l2 - l1 > 2) return false;
    return diff(prv, cur) == 1;
  }

  static void printChain(List<String> chain) {
    for (String word : chain) {
      System.out.println(word);
    }
  }

  // on-demand construction of word chain
  public static List<String>
      onDemand(Set<String> words, String src, String dst)
  {
    List<String> visited = new LinkedList<String>();
    visited.add(src);
    List<List<String>> chains = onDemandRecursive(words, visited, src, dst);

    List<String> res = null;
    int length = Integer.MAX_VALUE;
    for (List<String> chain : chains) {
      if (chain.size() < length) {
        length = chain.size();
        res = chain;
      }
    }
    if (res != null) printChain(res);
    return res;
  }

  static List<List<String>> onDemandRecursive(Set<String> words,
      List<String> visited, String src, String dst)
  {
    List<List<String>> res = new LinkedList<List<String>>();

    // base case: reach the destination
    if (src.equals(dst)) {
      List<String> chain = new LinkedList<String>();
      chain.add(dst);
      res.add(chain);
      return res;
    }

    // threshold
    if (visited.size() > dst.length() * 2) return res;

    // o.w., find a next word, which is linkable and towards the destination
    int old_distance = diff(src, dst);
    List<String> good_candidates = new LinkedList();
    List<String> fair_candidates = new LinkedList();
    for (String word : words) {
      if (visited.contains(word)) continue;
      if (!linkable(src, word)) continue;
      int new_distance = diff(word, dst);
      if (new_distance > old_distance) continue;

      if (new_distance == old_distance)
        fair_candidates.add(word);
      else // new_distance < old_distance
        good_candidates.add(word);
    }

    for (String word : good_candidates) {
      // recursively travel from the current word
      List<String> upd_visited = new LinkedList(visited);
      upd_visited.add(word);
      List<List<String>> sub_chains = onDemandRecursive(words, upd_visited, word, dst);
      // for each sub-chain
      for (List<String> sub_chain : sub_chains) {
        List<String> cp_chain = new LinkedList<String>(sub_chain);
        // append the source
        cp_chain.add(0, src);
        res.add(cp_chain);
      }
    }

    // if a chain found via a good candidate, stop here
    if (!res.isEmpty()) return res;

    // o.w. try fair candidates
    for (String word : fair_candidates) {
      // recursively travel from the current word
      List<String> upd_visited = new LinkedList(visited);
      upd_visited.add(word);
      List<List<String>> sub_chains = onDemandRecursive(words, upd_visited, word, dst);
      // for each sub-chain
      for (List<String> sub_chain : sub_chains) {
        List<String> cp_chain = new LinkedList<String>(sub_chain);
        // append the source
        cp_chain.add(0, src);
        res.add(cp_chain);
      }
    }

    return res;
  }

}
