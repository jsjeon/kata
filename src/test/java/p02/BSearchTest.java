package p02;

import static org.junit.Assert.*;
import org.junit.*;

import java.lang.reflect.Method;

public class BSearchTest {

  static String m1 = "chop_recur";
  static String m2 = "chop_iter";
  static String m3 = "chop_slice";

  static void testChop(String mname, int expected, int target, int[] arr) {
    Integer res = null;
    try {
      Method m = BSearch.class.getMethod(mname, int.class, int[].class);
      res = (Integer)m.invoke(null, target, arr);
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertNotNull(res);
    assertEquals(expected, res.intValue());
  }

  final static int[] empty = {};
  final static int[] one = { 1 };
  final static int[] three = { 1, 3, 5 };
  final static int[] four = { 1, 3, 5, 7 };

  static void testChopM(String m) {
    testChop(m, -1, 3, empty);
    testChop(m, -1, 3, one);
    testChop(m, 0, 1, one);

    testChop(m, 0, 1, three);
    testChop(m, 1, 3, three);
    testChop(m, 2, 5, three);

    testChop(m, -1, 0, three);
    testChop(m, -1, 2, three);
    testChop(m, -1, 4, three);
    testChop(m, -1, 6, three);

    testChop(m, 0, 1, four);
    testChop(m, 1, 3, four);
    testChop(m, 2, 5, four);
    testChop(m, 3, 7, four);

    testChop(m, -1, 0, four);
    testChop(m, -1, 2, four);
    testChop(m, -1, 4, four);
    testChop(m, -1, 6, four);
    testChop(m, -1, 8, four);
  }

  @Test
  public void testChopRecur() {
    testChopM(m1);
  }

  @Test
  public void testChopIter() {
    testChopM(m2);
  }

  @Test
  public void testChopSlice() {
    testChopM(m3);
  }

}
