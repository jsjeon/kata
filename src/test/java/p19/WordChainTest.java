package p19;

import util.WordReader;

import static org.junit.Assert.*;
import org.junit.*;

import java.util.List;
import java.util.Set;

public class WordChainTest {

  @Test
  public void testLinkable() {
    assertTrue(WordChain.linkable("cog", "dog")); // 1st letter
    assertTrue(WordChain.linkable("lead", "load")); // 2nd letter
    assertTrue(WordChain.linkable("ruby", "rubs")); // last letter
    assertTrue(WordChain.linkable("car", "card")); // inclusive
    assertTrue(WordChain.linkable("dirt", "dirty")); // inclusive
    assertFalse(WordChain.linkable("dirt", "dirten")); // not by one letter
  }

  static Set<String> words = null;

  @BeforeClass
  public static void setUp() {
    words = WordReader.readWords();
  }

  void testChain(List<String> chain) {
    for (int i = 0; i < chain.size() - 1; i++) {
      assertTrue(WordChain.linkable(chain.get(i), chain.get(i+1)));
    }
  }

  @Test
  public void testWordChainOnDemand1() {
    List<String> chain = WordChain.onDemand(words, "cat", "dog");
    assertEquals(4, chain.size());
    assertEquals("cat", chain.get(0));
    assertEquals("dog", chain.get(3));
    testChain(chain);
  }

  @Test
  public void testWordChainOnDemand2() {
    List<String> chain = WordChain.onDemand(words, "lead", "gold");
    assertEquals(4, chain.size());
    assertEquals("lead", chain.get(0));
    assertEquals("gold", chain.get(3));
    testChain(chain);
  }

  @Test
  public void testWordChainOnDemand3() {
    List<String> chain = WordChain.onDemand(words, "ruby", "code");
    assertEquals(6, chain.size());
    assertEquals("ruby", chain.get(0));
    assertEquals("code", chain.get(5));
    testChain(chain);
  }


  @Test
  public void testWordChainOnDemand4() {
    List<String> chain = WordChain.onDemand(words, "ruby", "gold");
    assertEquals(6, chain.size());
    assertEquals("ruby", chain.get(0));
    assertEquals("gold", chain.get(5));
    testChain(chain);
  }

}
