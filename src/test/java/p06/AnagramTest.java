package p06;

import util.WordReader;

import static org.junit.Assert.*;
import org.junit.*;

import java.util.Set;

public class AnagramTest {

  static Set<String> words = null;

  @BeforeClass
  public static void setUp() {
    words = WordReader.readWords();
  }

  @Test
  public void testCheck() {
    assertTrue(Anagram.check("kinship", "pinkish"));
    assertTrue(Anagram.check("enlist", "inlets", "listen", "silent"));
    assertTrue(Anagram.check("boaster", "boaters", "borates"));
    assertTrue(Anagram.check("fresher", "refresh"));
    assertTrue(Anagram.check("sinks", "skins"));
    assertTrue(Anagram.check("knits", "stink"));
    assertTrue(Anagram.check("rots", "sort"));

    assertTrue(Anagram.check("crepitus", "cuprites", "pictures", "piecrust"));
    assertTrue(Anagram.check("paste", "pates", "peats", "septa", "spate", "tapes", "tepas"));
    assertTrue(Anagram.check("punctilio", "unpolitic"));
    assertTrue(Anagram.check("sunders", "undress"));
  }

  @Test
  public void testAnagram1() {
    Set<String> anagrams = Anagram.find(words, "pictures");
    assertTrue(anagrams.contains("crepitus"));
    assertTrue(anagrams.contains("piecrust"));
  }

  @Test
  public void testAnagram2() {
    Set<String> anagrams = Anagram.find(words, "paste");
    assertTrue(anagrams.contains("septa"));
    assertTrue(anagrams.contains("spate"));
  }

  @Test
  public void testAnagram3() {
    Set<String> anagrams = Anagram.find(words, "listen");
    assertTrue(anagrams.contains("enlist"));
    assertTrue(anagrams.contains("silent"));
    assertTrue(anagrams.contains("tinsel"));
  }

}
