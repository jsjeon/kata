package p08;

import util.WordReader;
import util.Pair;
import util.StringUtil;

import static org.junit.Assert.*;
import org.junit.*;

import java.lang.reflect.Method;

import java.util.Set;
import java.util.List;

public class CompoundTest {

  static Set<String> words = null;

  @BeforeClass
  public static void setUp() {
    words = WordReader.readWords();
  }

  static String m1 = "decomposeExt";
  static String m2 = "decomposeFast";
  static String m3 = "decomposeReadable";

  static void testDecomposition(String mname, String s) {
    try {
      Method m = Compound.class.getMethod(mname, Set.class, String.class);
      Object res = m.invoke(null, words, s);
      try { // m2, m3
        Pair<String, String> decomp = (Pair<String, String>)res;
        System.out.println(s + " ~> " + decomp);
        assertEquals(decomp.car + decomp.cdr, s);
      } catch (ClassCastException ce) { // m1
        List<String> decomps = (List<String>)res;
        System.out.println(s + " ~> " + decomps.toString());
        assertEquals(StringUtil.join("", decomps.toArray()), s);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Extendible
   */

  @Test
  public void testExtendible1() {
    testDecomposition(m1, "albums");
  }

  @Test
  public void testExtendible2() {
    testDecomposition(m1, "barely");
  }

  @Test
  public void testExtendible3() {
    testDecomposition(m1, "befoul");
  }

  @Test
  public void testExtendible4() {
    testDecomposition(m1, "convex");
  }

  @Test
  public void testExtendible5() {
    testDecomposition(m1, "hereby");
  }

  @Test
  public void testExtendible6() {
    testDecomposition(m1, "jigsaw");
  }

  @Test
  public void testExtendible7() {
    testDecomposition(m1, "tailor");
  }

  @Test
  public void testExtendible8() {
    testDecomposition(m1, "weaver");
  }

  @Test // shorter
  public void testExtendible9() {
    testDecomposition(m1, "alone");
  }

  @Test // shorter
  public void testExtendible10() {
    testDecomposition(m1, "lobby");
  }

  @Test // longer
  public void testExtendible11() {
    testDecomposition(m1, "logically");
  }

  @Test // longer
  public void testExtendible12() {
    testDecomposition(m1, "waterproof");
  }

  /**
   * Fast
   */

  @Test
  public void testFast1() {
    testDecomposition(m2, "albums");
  }

  @Test
  public void testFast2() {
    testDecomposition(m2, "barely");
  }

  @Test
  public void testFast3() {
    testDecomposition(m2, "befoul");
  }

  @Test
  public void testFast4() {
    testDecomposition(m2, "convex");
  }

  @Test
  public void testFast5() {
    testDecomposition(m2, "hereby");
  }

  @Test
  public void testFast6() {
    testDecomposition(m2, "jigsaw");
  }

  @Test
  public void testFast7() {
    testDecomposition(m2, "tailor");
  }

  @Test
  public void testFast8() {
    testDecomposition(m2, "weaver");
  }

  /**
   * Readable
   */

  @Test
  public void testReadable1() {
    testDecomposition(m3, "albums");
  }

  @Test
  public void testReadable2() {
    testDecomposition(m3, "barely");
  }

  @Test
  public void testReadable3() {
    testDecomposition(m3, "befoul");
  }

  @Test
  public void testReadable4() {
    testDecomposition(m3, "convex");
  }

  @Test
  public void testReadable5() {
    testDecomposition(m3, "hereby");
  }

  @Test
  public void testReadable6() {
    testDecomposition(m3, "jigsaw");
  }

  @Test
  public void testReadable7() {
    testDecomposition(m3, "tailor");
  }

  @Test
  public void testReadable8() {
    testDecomposition(m3, "weaver");
  }

  @Test // shorter
  public void testReadable9() {
    testDecomposition(m3, "alone");
  }

  @Test // shorter
  public void testReadable10() {
    testDecomposition(m3, "lobby");
  }

  @Test // longer
  public void testReadable11() {
    testDecomposition(m3, "logically");
  }

  @Test // longer
  public void testReadable12() {
    testDecomposition(m3, "waterproof");
  }

}
