package util;

import static org.junit.Assert.*;
import org.junit.*;

import java.util.Set;

public class WordReaderTest {

  @Test
  public void testWordReader() {
    Set<String> words = WordReader.readWords();
    assertFalse(words.isEmpty());
  }

}
