package p21;

import static org.junit.Assert.*;
import org.junit.*;

public class AListTest {

  @Test
  public void testList1() {
    AList<String> lst = new AList<String>();
    assertNull(lst.find("fred"));

    lst.add("fred");
    assertEquals("fred", lst.find("fred").value());

    assertNull(lst.find("wilma"));

    lst.add("wilma");
    assertEquals("fred", lst.find("fred").value());
    assertEquals("wilma", lst.find("wilma").value());

    String[] all = {"fred", "wilma"};
    assertArrayEquals(all, lst.values());
  }

  @Test
  public void testList2() {
    AList<String> lst = new AList<String>();
    lst.add("fred");
    lst.add("wilma");
    lst.add("betty");
    lst.add("barney");
    String[] all = {"fred", "wilma", "betty", "barney"};
    assertArrayEquals(all, lst.values());

    lst.delete(lst.find("wilma"));
    String[] three = {"fred", "betty", "barney"};
    assertArrayEquals(three, lst.values());

    lst.delete(lst.find("barney"));
    String[] two = {"fred", "betty"};
    assertArrayEquals(two, lst.values());

    lst.delete(lst.find("fred"));
    String[] one = {"betty"};
    assertArrayEquals(one, lst.values());

    lst.delete(lst.find("betty"));
    String[] nope = {};
    assertArrayEquals(nope, lst.values());
  }

}
