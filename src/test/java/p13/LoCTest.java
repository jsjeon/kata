package p13;

import static org.junit.Assert.*;
import org.junit.*;

public class LoCTest {

  @Test
  public void testQuotes1() {
    String w = "This string includes \"inner quotation marks\"";
    String wo = "This string includes ";
    assertEquals(wo, LoC.rmQuotes(w));
  }

  @Test
  public void testQuotes2() {
    String w = "even\"multiple\" \"quotations\"";
    String wo = "even ";
    assertEquals(wo, LoC.rmQuotes(w));
  }

  static ClassLoader cl;

  @BeforeClass
  public static void setUp() {
    cl = LoC.class.getClassLoader();
  }

  static void testLoC(String f, int expected) {
    assertEquals(expected, LoC.count(f));
  }

  @Test
  public void testLoC1() {
    String f = cl.getResource("LoC1.java").getFile();
    testLoC(f, 3);
  }

  @Test
  public void testLoC2() {
    String f = cl.getResource("LoC2.java").getFile();
    testLoC(f, 5);
  }

}
